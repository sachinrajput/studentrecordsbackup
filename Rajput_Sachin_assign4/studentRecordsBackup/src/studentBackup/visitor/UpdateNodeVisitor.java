/**
 * 
 */
package studentBackup.visitor;

import studentBackup.bst.BST;
import studentBackup.util.Debug;

/**
 * @author sachin
 *
 */
public class UpdateNodeVisitor implements Visitor{

	private int updateValue;
	
	/***
	 * 
	 * @param updateValueIn
	 */
	public UpdateNodeVisitor(int updateValueIn){
		Debug.printDebug(1, "Constructor called for class UpdateNodeVisitor");
		setUpdateValue(updateValueIn);
	}
	
	/***
	 * visit used for implementing visitor pattern
	 * @param BST : the bst class
	 */
	@Override
	public void visit(BST bst) {
		// TODO Auto-generated method stub
		int value = getUpdateValue();
		bst.updateValueNodes(value);
	}
	
	/***
	 * 
	 * @return
	 */
	public int getUpdateValue() {
		return updateValue;
	}
	
	/***
	 * 
	 * @param updateValue
	 */
	public void setUpdateValue(int updateValue) {
		this.updateValue = updateValue;
	}
}
