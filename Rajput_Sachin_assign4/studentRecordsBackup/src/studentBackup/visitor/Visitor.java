/**
 * 
 */
package studentBackup.visitor;

import studentBackup.bst.BST;

/**
 * @author Sachin Rajput
 *
 */

public interface Visitor {
	public void visit(BST bst);
}
