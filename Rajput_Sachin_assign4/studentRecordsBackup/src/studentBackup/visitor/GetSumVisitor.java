/**
 * 
 */
package studentBackup.visitor;

import studentBackup.bst.BST;
import studentBackup.util.Debug;

/**
 * @author Sachin Rajput
 *
 */
public class GetSumVisitor implements Visitor {

	public GetSumVisitor(){
		Debug.printDebug(1, "Constructor called for class GetSumVisitor");
	}
	
	/***
	 * visit used for implementing visitor pattern
	 * @param BST : the bst class
	 */
	@Override
	public void visit(BST bst) {
		// TODO Auto-generated method stub
		bst.sumAllNodes();
	}

}
