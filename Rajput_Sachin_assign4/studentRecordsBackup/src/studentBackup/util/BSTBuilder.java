/**
 * 
 */
package studentBackup.util;

import java.io.BufferedReader;

import studentBackup.bst.BST;


/**
 * @author Sachin Rajput
 *
 */
public class BSTBuilder {
	
	private String fileName;
	BufferedReader br = null;
	private FileHelper fileHelper;
	
	public BST originalBST = new BST();
	public BST backup1BST = new BST();
	public BST backup2BST = new BST();
	
	/***
	 * 
	 * @param fileNameIn
	 */
	public BSTBuilder(String fileNameIn){
		Debug.printDebug(1, "Constructor called for class BSTBuilder");
		this.fileName = fileNameIn;
		this.fileHelper = new FileHelper(this.fileName);
		this.br = fileHelper.openFileRead();
	}

	/***
	 * 
	 */
	public void buildBST() {
		// TODO Auto-generated method stub
		try {
			String sCurrentLine;	
			while ((sCurrentLine = br.readLine()) != null) {
				int bNumber = 0;
				try {
					bNumber = Integer.parseInt(sCurrentLine);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					Debug.printDebug(2, "ERROR: Error parsing data in input file.");
				}
				originalBST.insert(bNumber,backup1BST,backup2BST);
			}
		} catch(Exception e){
			Debug.printDebug(2, "ERROR: Error parsing data in input file.");
		} finally {
			fileHelper.closeFile();
		}
	}
	
	
	
	
}
