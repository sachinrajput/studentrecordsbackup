/**
 * 
 */
package studentBackup.util;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/***
 * 
 * @author Sachin Rajput
 *
 */
public class FileHelper {
		
	public String fileName;
	public BufferedReader breader = null;
	public BufferedWriter bwriter = null;
	
	public FileHelper(String fileName) {
		Debug.printDebug(1, "Constructor called for class FileHelper");
		this.fileName = fileName;
	}

	
	/***
	 * This method opens the file
	 */
	public BufferedReader openFileRead() {
		try {
			breader = new BufferedReader(new FileReader(this.fileName));
			return breader;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Debug.printDebug(2, "ERROR: File Not Found: "+this.fileName);
		} finally {
			
		}
		return breader;
	}

	/***
	 * 
	 * @return
	 */
	public BufferedWriter openFileWrite() {
		try {
			bwriter = new BufferedWriter(new FileWriter(this.fileName));
			return bwriter;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Debug.printDebug(2, "ERROR: File Not Found: "+this.fileName);
		} finally {
			
		}
		return bwriter;
	}
	
	/***
	 * This method will close the file
	 */
	public void closeFile(){
		try {
			if (breader != null)breader.close();
			if (bwriter != null)bwriter.close();
		} catch (IOException ex) {
			Debug.printDebug(2, "ERROR: Closing file: "+this.fileName);
		} finally {
			
		}
	}
	
	/***
	 * Reads the input file.
	 */
	public void readFile() {

		String sCurrentLine;
		try {
			while ((sCurrentLine = breader.readLine()) != null) {
				// String details[] = sCurrentLine.split(" ");
				System.out.println(sCurrentLine);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Debug.printDebug(2, "ERROR: Reading file: "+this.fileName);
		} finally {

		}
	}
	
	
}
