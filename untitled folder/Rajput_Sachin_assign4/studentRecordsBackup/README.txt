CS542 Design Patterns
Fall 2013
PROJECT-4 README FILE

Due Date: Sunday, November 24, 2013
Submission Date: Sunday, November 24, 2013
Grace Period Used This Project: 0 Days
Grace Period Remaining: N/A
Author(s): Sachin Rajput 
e-mail(s): srajput1@binghamton.edu 


PURPOSE:

[
	This assignment helped me to learn the use of visitor and observer pattern.  
	JAVADOC is created: studentBackup/javadoc/index.html
]


PERCENT COMPLETE:

[
	I believe, we have completed 100% of this project.
]

PARTS THAT ARE NOT COMPLETE:

[
 N/A
]

BUGS:

[
 None
]

FILES:
[
	Driver.java: Main driver code resides in this class
	BST.java: This class is the base class for implementing all BST methods
	Node.java: This class is used for Node creation, update
	Observer.java: This is an interface for observer
	BSTBuilder.java: This class helps to create the bst's
	Debug.java: This is a Debug class
	FileHelper.java: This class helps in handling file methods
	GetSumVisitor.java: This is visitor to implement sum of all nodes
	InOrderTraversalVisitor.java: This is visitor to implement inorder printing
	UpdateNodeVisitor.java: This is visitor to implement update of nodes
	Visitor.java: This is an interface for visitor
	VisitorHelper.java: This class is helper for calling all visitors
]

SAMPLE OUTPUT:
[
 bingsuns2% ant run
 jar:
    [mkdir] Created dir: /import/linux/home/srajput1/dp/studentRecordsBackup/build/jar
      [jar] Building jar: /import/linux/home/srajput1/dp/studentRecordsBackup/build/jar/studentBackup.jar

run:
     [java] Inorder:
     [java] Original Tree:
     [java] 420213
     [java] 420217
     [java] 420219
     [java] 421213
     [java] 421217
     [java] 421219
     [java] 422219
     [java] 423219
     [java] 424219
     [java] 427219
     [java] 428219
     [java] 470212
     [java] 471212
     [java] 480219
     [java] 481219
     [java] Backup Tree 1:
     [java] 420213
     [java] 420217
     [java] 420219
     [java] 421213
     [java] 421217
     [java] 421219
     [java] 422219
     [java] 423219
     [java] 424219
     [java] 427219
     [java] 428219
     [java] 470212
     [java] 471212
     [java] 480219
     [java] 481219
     [java] Backup Tree 2:
     [java] 420213
     [java] 420217
     [java] 420219
     [java] 421213
     [java] 421217
     [java] 421219
     [java] 422219
     [java] 423219
     [java] 424219
     [java] 427219
     [java] 428219
     [java] 470212
     [java] 471212
     [java] 480219
     [java] 481219
     [java] 
     [java] Sum:
     [java] Sum of Original BST:
     [java] 3905960
     [java] Sum of backup1BST BST:
     [java] 3905960
     [java] Sum of backup2BST BST:
     [java] 3905960
     [java] 
     [java] Inorder after update:
     [java] 
     [java] Inorder:
     [java] Original Tree:
     [java] 420217
     [java] 420221
     [java] 420223
     [java] 421217
     [java] 421221
     [java] 421223
     [java] 422223
     [java] 423223
     [java] 424223
     [java] 427223
     [java] 428223
     [java] 470216
     [java] 471216
     [java] 480223
     [java] 481223
     [java] Backup Tree 1:
     [java] 420217
     [java] 420221
     [java] 420223
     [java] 421217
     [java] 421221
     [java] 421223
     [java] 422223
     [java] 423223
     [java] 424223
     [java] 427223
     [java] 428223
     [java] 470216
     [java] 471216
     [java] 480223
     [java] 481223
     [java] Backup Tree 2:
     [java] 420217
     [java] 420221
     [java] 420223
     [java] 421217
     [java] 421221
     [java] 421223
     [java] 422223
     [java] 423223
     [java] 424223
     [java] 427223
     [java] 428223
     [java] 470216
     [java] 471216
     [java] 480223
     [java] 481223
     [java] 
     [java] Sum:
     [java] Sum of Original BST:
     [java] 3905996
     [java] Sum of backup1BST BST:
     [java] 3905996
     [java] Sum of backup2BST BST:
     [java] 3905996
     [java] 

BUILD SUCCESSFUL
Total time: 2 seconds
]

TO COMPILE:
[
 tar -xvf Rajput_Sachin_assign4.tar 
 cd Rajput_Sachin_assign4
 ant compile
]

TO RUN:
[
   ant run
   Note: to change the inputs open build.xml look for :
   <target name="run" depends="jar">
   	<java jar="${BUILD}/jar/studentBackup.jar" fork="true">
   		<arg value="bstInput.txt" />
   		<arg value="4" />
   		<arg value="0" />
   	</java>
   </target>
]

DEBUG LEVELS:
[

 0: Only output is printed.
 1: Print all constructor calls
 2: To see error messages if exist
]

EXTRA CREDIT:
[
 N/A
]


BIBLIOGRAPHY:
This serves as evidence that we are in no way intending Academic Dishonesty.
<Sachin Rajput>

[
. JAVA API Documentation (http://docs.oracle.com/javase/7/docs/api/)
. Complete Reference - JAVA
]

