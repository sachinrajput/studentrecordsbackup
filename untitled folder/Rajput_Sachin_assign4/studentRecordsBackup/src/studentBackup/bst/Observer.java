/**
 * 
 */
package studentBackup.bst;

/**
 * @author Sachin Rajput
 *
 */
public interface Observer {
	public void update(int updateValue);
}
