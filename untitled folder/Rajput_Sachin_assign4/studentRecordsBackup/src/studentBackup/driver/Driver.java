/**
 * 
 */
package studentBackup.driver;

import studentBackup.util.Debug;
import studentBackup.util.BSTBuilder;
import studentBackup.visitor.VisitorHelper;

/**
 * @author Sachin Rajput
 * 
 */
public class Driver {

	private static int debugValue = 0; // Default Debug Value
	public static String inputFileName = "";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		inputFileName = args[0];
		int updateValue = Integer.parseInt(args[1]);

		try {
			if (args.length > 2)
				debugValue = Integer.parseInt(args[2]);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			Debug.checkInput(args.length, args);
		}

		Debug.setDebugValue(debugValue);
		Debug.checkInput(args.length, args);


		BSTBuilder bstBuilder = new BSTBuilder(inputFileName);
		bstBuilder.buildBST();

		// this class is just a helper to invoke visitor's
		VisitorHelper visitorHelper = new VisitorHelper(bstBuilder);

		visitorHelper.printInOrder();

		visitorHelper.printSum();

		visitorHelper.updateValue(updateValue);

		visitorHelper.printInOrder();

		visitorHelper.printSum();
	}

}
