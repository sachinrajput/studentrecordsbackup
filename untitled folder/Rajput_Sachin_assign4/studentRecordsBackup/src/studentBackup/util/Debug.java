/**
 * 
 */
package studentBackup.util;

import java.io.File;



/**
 * @author Sachin Rajput Class Debug - used for logging
 *         purpose.
 */

public class Debug {

	private static int DEBUG_VALUE;

	/**
	 * Setter method(mutator) for DEBUG_VALUE
	 * 
	 * @param value
	 *            - set the DEBUG_VALUE to this
	 */

	public static void setDebugValue(int value) {

		DEBUG_VALUE = value;

	}

	/**
	 * Getter(accessor) for DEBUG_VALUE
	 * 
	 * @return - returns the DEBUG_VALUE
	 */

	public static int getDebugValue() {

		return DEBUG_VALUE;

	}

	public static void checkInput(int length, String[] args) {
		if (length < 2) {
			if(length == 0)
				Debug.printDebug("ERROR: No arguments passed, follow the below format: \nInputFileName, UPDATE_VALUE, DebugLevel");
				Debug.printDebug(2, "ERROR: Invalid number of arguments, follow the below format: \nInputFileName, UPDATE_VALUE, DebugLevel");
		} else if (length == 2) {
			Debug.printDebug("WARNING: No Debug Input parameter passed.\nDefault DEBUG Level set to 0.");
		} else if (Debug.getDebugValue() < 0 || Debug.getDebugValue() > 2) {
			Debug.printDebug("ERROR: Debug value must be between 0-3");
		}
		
		if(length == 3 || length == 2){
			try{
				if(length == 3){
						Integer.parseInt(args[2]);
				}
			} catch (NumberFormatException e) {
				Debug.printDebug("ERROR:Input parameter should be a number!\nDefault DEBUG Level set to 0.");
			}
			
			if(!args[0].contains(".txt")){
				//Debug.printDebug(0, "ERROR: The Input file "+args[0]+" should have .txt format.");
				Debug.printDebug("ERROR: The Input file "+args[0]+" should have .txt format.");
			}
			
			File f = new File(args[0]);
			if(!f.exists()){
				Debug.printDebug("ERROR: The Input file "+args[0]+" does not exist.");
			}
		}
	}

	/**
	 * Prints the contents as per the debug value
	 * 
	 * @param value
	 *            - DEBUG_VALUE
	 * @param Message
	 *            - message to print
	 */

	public static void printDebug(int value, String Message) {

		if (getDebugValue() == value) {
			switch (value) {
			case 2:
				System.err.println(Message);
				System.exit(-1);
				break;
			default:
				System.out.println(Message);
				break;
			}
		} else {
			if(value == 2){
				printDebug(Message);
				System.exit(-1);
			}
		}
	}
	
	/**
	 * Prints the error message when the debug value is not set
	 * @param Message
	 */
	public static void printDebug(String Message){
		System.err.println(Message);
		//System.exit(-1);
	}

	/***
	 * 
	 */
	@Override
	public String toString() {
		return "Debug []";
	}
	
	
}
