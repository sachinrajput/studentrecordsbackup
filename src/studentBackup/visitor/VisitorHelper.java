/**
 * 
 */
package studentBackup.visitor;

import studentBackup.bst.BST;
import studentBackup.util.BSTBuilder;
import studentBackup.util.Debug;

/**
 * @author Sachin Rajput
 *
 */
public class VisitorHelper {
	
	private BST originalBST,backup1BST,backup2BST;
	
	/***
	 * 
	 * @param bstBuilderIn
	 */
	public VisitorHelper(BSTBuilder bstBuilderIn){
		originalBST = bstBuilderIn.originalBST;
		backup1BST = bstBuilderIn.backup1BST;
		backup2BST = bstBuilderIn.backup2BST;
	}
	
	/***
	 * Visitor for printing Inorder
	 */
	public void printInOrder(){
		Debug.printDebug(0,"Inorder:");
		InOrderTraversalVisitor inOrder = new InOrderTraversalVisitor();
		Debug.printDebug(0,"Original Tree:");
		originalBST.accept(inOrder);
		Debug.printDebug(0,"Backup Tree 1:");
		backup1BST.accept(inOrder);
		Debug.printDebug(0,"Backup Tree 2:");
		backup2BST.accept(inOrder);
		Debug.printDebug(0,"");
	}
	
	/***
	 * Visitor for adding all nodes
	 */
	public void printSum(){
		Debug.printDebug(0,"Sum:");
		GetSumVisitor getSum = new GetSumVisitor();
		Debug.printDebug(0,"Sum of Original BST:");
		originalBST.accept(getSum);
		Debug.printDebug(0,"Sum of backup1BST BST:");
		backup1BST.accept(getSum);
		Debug.printDebug(0,"Sum of backup2BST BST:");
		backup2BST.accept(getSum);
		Debug.printDebug(0,"");
	}
	
	/***
	 * Visitor for updating node values.
	 * Use observer pattern to notify the observers
	 * 
	 * @param updateValue
	 */
	public void updateValue(int updateValue){
		Debug.printDebug(0,"Inorder after update:");
		UpdateNodeVisitor updateNode = new UpdateNodeVisitor(updateValue);
		originalBST.accept(updateNode);
		Debug.printDebug(0,"");
	}
	
	
}
