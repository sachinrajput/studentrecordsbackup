/**
 * 
 */
package studentBackup.visitor;

import studentBackup.bst.BST;
import studentBackup.util.Debug;

/**
 * @author sachin
 *
 */
public class InOrderTraversalVisitor implements Visitor {

	public InOrderTraversalVisitor(){
		Debug.printDebug(1, "Constructor called for class InOrderTraversalVisitor");
	}
	
	/***
	 * visit used for implementing visitor pattern
	 */
	@Override
	public void visit(BST bst) {
		// TODO Auto-generated method stub
		bst.inOrderTraversal();
	}

}
