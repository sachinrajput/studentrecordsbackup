/**
 * 
 */
package studentBackup.bst;

import studentBackup.util.Debug;
import studentBackup.visitor.Visitor;



/**
 * @author Sachin Rajput
 *
 */
public class BST {
	
	public Node root;
	private int sumAllNodes;
	
	private BST backup1;
	private BST backup2;
	
	public BST(){
		Debug.printDebug(1, "Constructor called for class BST");
	}
	
	/***
	 * insert is used to insert values in BST
	 * @param bNumberIn
	 * @param backup1In
	 * @param backup2In
	 */
	public void insert(int bNumberIn,BST backup1In,BST backup2In){
		backup1 = backup1In;
		backup2 = backup2In;
		//create node
		if(root == null){
			root = new Node(bNumberIn);
			backup1.root = new Node(root,bNumberIn);
			backup2.root = new Node(root,bNumberIn);
		} else {
			//call recursive method
			insert_helper(root,bNumberIn,backup1.root,backup2.root);
		}
	}

	/***
	 * insert_helper method helps as a helper method for insert
	 * @param nodeIn
	 * @param bNumberIn
	 * @param nodebackup1In
	 * @param nodebackup2In
	 */
	private void insert_helper(Node nodeIn, int bNumberIn, Node nodebackup1In,Node nodebackup2In) {
		// TODO Auto-generated method stub
		int compare = compareInt(nodeIn.getbNumber(),bNumberIn);
		if(compare == 1){
			if(nodeIn.left == null){
				nodeIn.left = new Node(bNumberIn);
				nodebackup1In.left = new Node(nodeIn.left,bNumberIn);
				nodebackup2In.left = new Node(nodeIn.left,bNumberIn);
			} else
				insert_helper(nodeIn.left, bNumberIn,nodebackup1In.left,nodebackup2In.left);
		} else if(compare == 0){
			if(nodeIn.right == null){
				nodeIn.right = new Node(bNumberIn);
				nodebackup1In.right = new Node(nodeIn.right,bNumberIn);
				nodebackup2In.right = new Node(nodeIn.right,bNumberIn);
			} else
				insert_helper(nodeIn.right, bNumberIn,nodebackup1In.right,nodebackup2In.right);
		} 
	}

	/***
	 * compareInt compares two integer numbers
	 * @param getbNumber
	 * @param bNumberIn
	 * @return
	 */
	private int compareInt(int getbNumber, int bNumberIn) {
		// TODO Auto-generated method stub
		if(getbNumber>bNumberIn){ //Move left
			return 1; 
		} else if(getbNumber<bNumberIn){ //Move right
			return 0;
		}
		return -1; //Duplicate
	}
	
	/***
	 * This method is used for Visitor pattern
	 * @param visit
	 */
	public void accept(Visitor visit){
		visit.visit(this);
	}
	
	/***
	 * Print inorder
	 */
	public void inOrderTraversal(){
		inOrderTraversal_r(root);
	}
	
	/***
	 * Helper method for inorder
	 * @param nodeIn
	 */
	public void inOrderTraversal_r(Node nodeIn){
		// TODO Auto-generated method stub
		if(nodeIn == null) return;
		inOrderTraversal_r(nodeIn.left);
		Debug.printDebug(0,nodeIn.toString());
		inOrderTraversal_r(nodeIn.right);
	}
	
	/***
	 * adding all node values
	 */
	public void sumAllNodes(){
		setSumAllNodes(sumAllNodes_helper(root));
		Debug.printDebug(0,this.toString());
	}

	/***
	 * helper method for adding all node values 
	 * @param nodeIn
	 * @return
	 */
	private int sumAllNodes_helper(Node nodeIn) {
		// TODO Auto-generated method stub
		if(nodeIn==null) return 0;
		if( (nodeIn.left==null) && (nodeIn.right==null)) return 0; 
		return nodeIn.getbNumber()+sumAllNodes_helper(nodeIn.left)+sumAllNodes_helper(nodeIn.right);
	}

	/***
	 * 
	 * @return
	 */
	public int getSumAllNodes() {
		return sumAllNodes;
	}

	/***
	 * 
	 * @param sumAllNodes
	 */
	public void setSumAllNodes(int sumAllNodes) {
		this.sumAllNodes = sumAllNodes;
	}
	
	/***
	 * 
	 * @param updateValue
	 */
	public void updateValueNodes(int updateValue){		
		updateValueNodes(root,updateValue);
	}

	/***
	 * 
	 * @param nodeIn
	 * @param updateValue
	 */
	private void updateValueNodes(Node nodeIn, int updateValue) {
		// TODO Auto-generated method stub
		if(nodeIn==null) return;
		nodeIn.setbNumber(nodeIn.getbNumber()+updateValue);
		nodeIn.setUpdateValue(updateValue);
		nodeIn.notifyObserver();
		updateValueNodes(nodeIn.left,updateValue);
		updateValueNodes(nodeIn.right,updateValue);
	}

	/***
	 * 
	 */
	@Override
	public String toString() {
		return "" + sumAllNodes;
	}

	
}
