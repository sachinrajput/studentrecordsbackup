/**
 * 
 */
package studentBackup.bst;

/**
 * @author Sachin Rajput
 *
 */
public interface Subject {
	public void registerObserver(Observer o);
	public void removeObserver(Observer o);
	public void notifyObserver();
}
