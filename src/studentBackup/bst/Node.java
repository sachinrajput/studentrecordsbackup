/**
 * 
 */
package studentBackup.bst;

import java.util.ArrayList;

import studentBackup.util.Debug;

/**
 * @author Sachin Rajput
 *
 */
public class Node implements Subject,Observer {
	

	private int bNumber;
	public Node left;
	public Node right;
	
	private ArrayList<Observer> observers;
	private Subject nodeSubject;
	private int updateValue;
	
	public Node(){
		Debug.printDebug(1, "Constructor called for class Node()");
		observers = new ArrayList<Observer>();
	}
	
	/***
	 * 
	 * @param nodeSubjectIn
	 * @param bNumberIn
	 */
	public Node(Subject nodeSubjectIn,int bNumberIn){
		Debug.printDebug(1, "Constructor called for class Node(Subject nodeSubjectIn,int bNumberIn)");
		setNodeSubject(nodeSubjectIn);
		setbNumber(bNumberIn);
		getNodeSubject().registerObserver(this);
	}
	
	/***
	 * 
	 * @param bNumberIn
	 */
	public Node(int bNumberIn){
		setbNumber(bNumberIn);
	}


	/***
	 * update used for implementing observer pattern
	 * called when the subject is updated 
	 */
	@Override
	public void update(int updateValueIn) {
		// TODO Auto-generated method stub
		setbNumber(updateValueIn+getbNumber());
	}

	/***
	 * registerObserver used for implementing observer pattern
	 * this is called when an observer wants to register itself
	 */
	@Override
	public void registerObserver(Observer o) {
		// TODO Auto-generated method stub
		getobservers().add(o);
	}

	/***
	 * removeObserver used for implementing observer pattern
	 * this is called when an observer wants to remove itself
	 */
	@Override
	public void removeObserver(Observer o) {
		// TODO Auto-generated method stub
		int i = getobservers().indexOf(o);
		if(i>=0){
			getobservers().remove(i);
		}
	}

	/***
	 * notifyObserver used for implementing observer pattern
	 * this is called when we want to notify all observers
	 */
	@Override
	public void notifyObserver() {
		// TODO Auto-generated method stub
		for(int i = 0;i < getobservers().size();i++){
			Observer observer = (Observer)getobservers().get(i);
			observer.update(getUpdateValue());
		}
	}
	
	/***
	 * 
	 */
	@Override
	public String toString() {
		return ""+bNumber;
	}

	/***
	 * 
	 * @return
	 */
	public int getUpdateValue() {
		return updateValue;
	}

	/***
	 * 
	 * @param updateValue
	 */
	public void setUpdateValue(int updateValue) {
		this.updateValue = updateValue;
	}
	
	/***
	 * 
	 * @return
	 */
	public int getbNumber() {
		return bNumber;
	}

	/***
	 * 
	 * @param bNumber
	 */
	public void setbNumber(int bNumber) { 
		this.bNumber = bNumber;
	}

	/***
	 * 
	 * @return
	 */
	public ArrayList<Observer> getobservers() {
		if(observers == null) 
			observers = new ArrayList<Observer>();
		return observers;
	}

	/***
	 * 
	 * @param observersIn
	 */
	public void setobservers(ArrayList<Observer> observersIn) {
		this.observers = observersIn;
	}

	/***
	 * 
	 * @return
	 */
	public Subject getNodeSubject() {
		return nodeSubject;
	}

	/***
	 * 
	 * @param nodeSubject
	 */
	public void setNodeSubject(Subject nodeSubject) {
		this.nodeSubject = nodeSubject;
	}
	
}
